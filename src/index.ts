import Bestor from "./lib/bestor";
import Component from "./lib/component";
import Data from "./lib/data";
import Utils from "./lib/utils";

export {
    Bestor,
    Component,
    Data,
    Utils
}

export default Bestor;