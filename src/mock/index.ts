import {Bestor} from "../index";
import Home from "./page/home";

const start = new Date().getTime();
Bestor.$app(Home, 'body');
const end = new Date().getTime();
setTimeout(() => {
    console.log('run time', (end - start) / 1000, 's');
});