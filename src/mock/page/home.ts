import {Component, Data, Utils} from "../../index";

export default class Home extends Component {
    list = Data.reactive<Array<any>>(
        new Array(100).fill('').map((k, i) => ({text: Utils.uniqueId('text')}))
    );

    render() {
        return this.$c('section', [
            this.$c('button', {
                text: '新增',
                on: {
                    click: (e) => {
                        this.list.push({
                            text: new Date().getTime()
                        });
                        console.log('button click');
                    }
                },
            }),
            this.$c('ul', this.list.map((item: any, index: number) => {
                return this.$c('li', {
                    on: {
                        click: (e) => {
                            this.list.splice(index, 1);
                            console.log('ul click');
                        }
                    },
                    bind: {
                        style: {
                            color: 'red'
                        },
                    },
                    text: item.text,
                });
            }))
        ]);
    }
}