import {Key} from "./type";
import Patch from "./patch";

export default class Data {
    public static reactive = <T extends Record<Key, any>>(value: Record<Key, any>): T => {
        return new Proxy(value, {
            set(target: any, p: Key, value: any, receiver: any): boolean {
                target[p] = value;
                return true;
            },
            get(target: any, action: any, receiver: any): any {
                return function (...params: any) {
                    return Patch.proxyHandler({target, action, params});
                }
            }
        });
    };
}