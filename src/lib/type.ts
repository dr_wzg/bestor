export type Key = string;
export type On = Record<string, (...arg: any) => any>;
export type Dom = Element | HTMLElement;

export interface Bind extends Record<string, any> {
    class?: string | string[];
    style?: Partial<CSSStyleDeclaration>;
}

export interface HashTreeItem {
    key: Key;
    tag?: string;
    parent?: Key;
    children?: Key[];
    left?: Key;
    right?: Key;
    text?: string;
    bind?: Bind;
    on?: On;
}

export interface HashDomItem {
    key: Key;
    dom: Dom;
}

export type HashTreeMap = Map<Key, HashTreeItem>;
export type HashDomMap = Map<Key, HashDomItem>;
export type HashKeyMap = Map<Key, number>;

export interface DbMap {
    TREE: HashTreeMap;
    DOM: HashDomMap;
    KEY: HashKeyMap;
}

export type MapTarget = HashTreeMap | HashDomMap | HashKeyMap;
export type ProxyMapAction = keyof Map<Key, MapTarget> | keyof Array<any> | string | symbol;

export interface ProxyMapOptions<T> {
    action: ProxyMapAction;
    target: T;
    params: any[];
}

export interface ElementData extends Record<string, any> {
    text?: string;
    bind?: Bind;
    on?: On;
}

export type CreateHashTree = (tag: string, data?: string | ElementData | HashTreeItem[], children?: HashTreeItem[]) => HashTreeItem;