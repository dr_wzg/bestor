import {DbMap, HashDomMap, HashKeyMap, HashTreeMap, ProxyMapAction} from "./type";
import Patch from "./patch";

export default class Db {
    public static MAP: DbMap = {
        TREE: new Map(),
        DOM: new Map(),
        KEY: new Map(),
    };
    public static treeMap: HashTreeMap = new Proxy(Db.MAP.TREE, {
        set(target: HashTreeMap, p: string | symbol, value: any, receiver: any): boolean {
            return false;
        },
        get(target: HashTreeMap, action: ProxyMapAction, receiver: any): any {
            return function (...params: any) {
                return Patch.proxyHandler({target, action, params});
            }
        }
    });

    public static domMap: HashDomMap = new Proxy(Db.MAP.DOM, {
        set(target: HashDomMap, p: string | symbol, value: any, receiver: any): boolean {
            return false;
        },
        get(target: HashDomMap, action: ProxyMapAction, receiver: any): any {
            return function (...params: any) {
                return Patch.proxyHandler({target, action, params});
            }
        }
    });

    public static keyMap: HashKeyMap = new Proxy(Db.MAP.KEY, {
        set(target: HashKeyMap, p: string | symbol, value: any, receiver: any): boolean {
            return false;
        },
        get(target: any, action: ProxyMapAction, receiver: any): any {
            return function (...params: any) {
                return Patch.proxyHandler({target, action, params});
            }
        }
    });
}