import Db from "./db";
import {HashDomItem, Key} from "./type";
import HashDom from "./hash-dom";
import {SYSTEM_CONST} from "./const";

export default class Render {
    static $vnode(parent: Key) {
        const parentDom = Db.domMap.get(parent);
        if (!parentDom) {
            return;
        }
        Render.removeInvalidDom(parent);
        Array.from(Db.treeMap.entries() || [])
            .filter(([key, item]) => item.parent === parent)
            .forEach(([key, item]) => {
                let hashDom: HashDomItem = Db.domMap.get(key);
                if (!hashDom) {
                    hashDom = HashDom.create({key});
                }
                parentDom.dom?.append(hashDom.dom);
                Render.$vnode(key);
            });
        return parentDom;
    }

    public static getRealDom(key: Key) {
        return document.querySelector('[' + SYSTEM_CONST.BIND_KEY_NAME + '=' + key + ']');
    }

    public static removeInvalidDom(key: Key) {
        return Render.getRealDom(key)?.querySelectorAll(':not([' + SYSTEM_CONST.BIND_KEY_NAME + '=' + key + '])')?.forEach(item => item?.remove());
    }
}