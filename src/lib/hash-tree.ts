import Db from "./db";
import {Bind, HashTreeItem, Key, On} from "./type";
import Utils from "./utils";
import {SYSTEM_CONST} from "./const";

export default class HashTree implements HashTreeItem {
    public key!: Key;
    public tag!: string;
    public text!: string;
    public bind!: Bind;
    public on!: On;
    public parent!: Key;
    public children!: Key[];
    public left!: Key;
    public right!: Key;

    constructor(options: Partial<HashTreeItem>) {
        Object.assign(this, options);
        if (!this.key) {
            this.key = Utils.uniqueId(SYSTEM_CONST.KEY_PREFIX);
        }
        if (!this.tag) {
            this.tag = this.key;
        }
        this.children?.forEach((k, i) => {
            const child = Db.treeMap.get(k);
            if (child) {
                const left = this.children?.[i - 1];
                const right = this.children?.[i + 1];
                Db.treeMap.set(child.key, {...child, parent: this.key, left, right});
            }
        });
        Db.treeMap.set(this.key, this);
    }

    public static create(options: Partial<HashTreeItem>) {
        return new HashTree(options);
    }
}