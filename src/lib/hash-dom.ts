import {Dom, HashDomItem, Key} from "./type";
import Db from "./db";
import Utils from "./utils";
import {SYSTEM_CONST} from "./const";

export default class HashDom implements HashDomItem {
    public key!: Key;
    public dom!: Dom;

    constructor(options: Partial<HashDomItem>) {
        Object.assign(this, options);
        if (!this.key) {
            this.key = Utils.uniqueId(SYSTEM_CONST.KEY_PREFIX);
        }
        if (!this.dom) {
            this.dom = HashDom.build(this.key);
        }
        Db.domMap.set(this.key, this);
    }

    public static create(options: Partial<HashDomItem>) {
        return new HashDom(options)
    }

    public static build(key: Key): Dom {
        const tree = Db.treeMap.get(key);
        if (!tree) {
            return;
        }
        const dom = document.createElement(tree.tag);
        Object.entries(tree.bind || {}).forEach(([prop, value]) => {
            switch (prop) {
                case 'class':
                    HashDom.handleDomClass(dom, value);
                    break;
                case 'style':
                    HashDom.handleDomStyle(dom, value);
                    break;
                default:
                    dom.setAttribute(prop, value);
                    break;
            }
        });
        dom.setAttribute(SYSTEM_CONST.BIND_KEY_NAME, key);
        Object.entries(tree.on || {}).forEach(([prop, value]) => {
            dom.addEventListener(prop, value);
        });
        if (![undefined, null].includes(tree.text)) {
            dom.append(tree.text);
        }
        return dom;
    }

    /**
     * 处理Dom class属性
     * @param dom
     * @param value
     */
    public static handleDomClass(dom: Element, value: any) {
        switch (Object.prototype.toString.call(value)) {
            case '[object String]':
                value.split(/\s/).forEach((cls: string) => {
                    dom.classList.add(cls);
                });
                break;
            case '[object Array]':
                value.forEach((cls: any) => {
                    HashDom.handleDomClass(dom, cls);
                });
                break;
            default:
                break;
        }
    }

    /**
     * 处理Dom样式
     * @param dom
     * @param value
     */
    public static handleDomStyle(dom: HTMLElement, value: any) {
        switch (Object.prototype.toString.call(value)) {
            case '[object String]':
                dom.setAttribute('style', value);
                break;
            case '[object Object]':
                Object.assign(dom.style, value);
                break;
            default:
                break;
        }
    }
}