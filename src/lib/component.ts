import Bestor from "./bestor";
import {CreateHashTree} from "./type";

export default class Component {
    constructor() {
    }

    public $c: CreateHashTree = Bestor.$c;

    public render($c?: CreateHashTree) {
        return this.$c('div', 'Hello Bestor!')
    }
}