import Db from "./db";
import {MapTarget, ProxyMapOptions} from "./type";
import Utils from "./utils";

export default class Patch {
    static proxyHandler(options: ProxyMapOptions<MapTarget>) {
        const {target, action, params} = options;
        const [key, value] = params;
        // @ts-ignore
        const result = Utils.execute((target[action] as any).bind(target), ...params);
        switch (target) {
            case Db.MAP.TREE:
                switch (action) {
                    case "set":
                        break;
                    case "delete":
                        break;
                }
                break;
            case Db.MAP.DOM:
                switch (action) {
                    case "set":
                        break;
                    case "delete":
                        break;
                }
                break;
            case Db.MAP.KEY:
                switch (action) {
                    case "set":
                        break;
                    case "delete":
                        break;
                }
                break;
            default:
                if (Utils.isArray(target)) {
                    switch (action) {
                        case "splice":
                        case "push":
                        case "pop":
                            console.log('Patch[proxyHandler] data array', {target, action, params});
                            break;
                        default:
                            break;
                    }
                }
                break;
        }
        return result;
    }
}