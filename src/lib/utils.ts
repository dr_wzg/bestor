import Db from "./db";

export default class Utils {
    public static uniqueId(prefix: string = '') {
        const v = Db.keyMap.get(prefix);
        const index = v ? v : 0;
        const next = index + 1;
        Db.keyMap.set(prefix, next);
        return [prefix, next].join('_')
    }

    public static execute(fn: any, ...params: any) {
        return Utils.isFunction(fn) ? fn(...params) : fn;
    }

    public static isType(data: any, type: string) {
        return Object.prototype.toString.call(data) === '[object ' + type + ']'
    }

    public static isArray(data: any) {
        return Utils.isType(data, 'Array');
    }

    public static isObject(data: any) {
        return Utils.isType(data, 'Object');
    }

    public static isString(data: any) {
        return Utils.isType(data, 'String');
    }

    public static isFunction(data: any) {
        return Utils.isType(data, 'Function');
    }
}