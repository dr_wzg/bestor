import {CreateHashTree, Dom, ElementData, HashTreeItem} from "./type";
import HashTree from "./hash-tree";
import Utils from "./utils";
import {SYSTEM_CONST} from "./const";
import HashDom from "./hash-dom";
import Render from "./render";
import Component from "./component";

export default class Bestor {
    public static $c: CreateHashTree = function (tag, data, children) {
        let tree = HashTree.create({tag});
        if (Utils.isString(data)) {
            tree = HashTree.create({...tree, text: data as string});
        } else if (Utils.isObject(data)) {
            tree = HashTree.create({...tree, ...(data as ElementData)});
        } else if (Utils.isArray(data)) {
            tree = HashTree.create({...tree, children: (data as [HashTreeItem]).map(children => children.key)});
        }
        if (!Utils.isArray(data) && Utils.isArray(children)) {
            tree = HashTree.create({...tree, children: children.map(child => child.key)});
        }
        return tree;
    };

    public static $app(component: typeof Component, selector: string | Dom = 'body') {
        let dom!: Dom;
        let key = Utils.uniqueId(SYSTEM_CONST.KEY_PREFIX);
        if (Utils.isString(selector)) {
            dom = document.querySelector(selector as string);
        } else {
            dom = selector as Dom;
        }
        dom.setAttribute(SYSTEM_CONST.BIND_KEY_NAME, key);
        const tree = Bestor.$parse(component);
        console.log('$app tree', tree);
        HashTree.create({...tree, parent: key});
        HashDom.create({key, dom});
        Render.$vnode(key);
    }

    public static $parse(component: typeof Component) {
        return new component().render(Bestor.$c);
    }
}