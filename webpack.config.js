const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const path = require('path');
const packageJson = require('./package');

module.exports = (env, argv) => {
    const config = {
        mode: argv.mode,
        target: ['web', 'es5'],
        output: {
            filename: "index.js",
            libraryTarget: "umd",
            path: path.resolve(process.cwd(), 'dist'),
            publicPath: '',
        },
        resolve: {
            extensions: ['.js', '.ts', '.json']
        },
        module: {
            rules: [{test: /\.ts$/, use: 'ts-loader'}],
        },
        plugins: []
    };
    switch (argv.mode) {
        case 'development':
            config.entry = path.resolve(__dirname, 'src/mock/index.ts');
            config.devServer = {
                port: 2022
            };
            config.plugins = [
                new HtmlWebpackPlugin({
                    title: packageJson.name,
                    filename: "index.html",
                    template: path.resolve(process.cwd(), 'public/index.html')
                })
            ];
            break;
        case 'production':
            config.entry = path.resolve(__dirname, 'src/index.ts');
            config.plugins = [
                new CleanWebpackPlugin(),
            ];
            break;
        default:
            break;
    }
    return config;
};