# Bestor

#### 介绍
Bestor极致的前端开发框架。

#### 软件架构
![设计原理](https://gitee.com/dr_wzg/bestor/raw/master/public/imagedesign-principle.png)


#### 安装教程

```
npm i best -S
```

#### 使用说明

```
git clone https://gitee.com/dr_wzg/bestor.git
cd bestor
npm i
npm run serve
```
